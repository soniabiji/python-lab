
def add_ing(str):
    if str[-3:] == 'ing': 
        return str + 'ly' 
    else:
        return str + 'ing' 

print("Enter a string: ")
str = input()
print("The string is ",str)
print("The string after adding 'ing'/'ly' is: ", add_ing(str)) 
