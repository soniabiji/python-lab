
def first_and_last_color(color_list):
    print("First color in the list is:",color_list[0]) 
    print("Last color in the list is:",color_list[-1]) 

print("Enter a list of colors separated by commas:")
color_list = input().split(",") 
first_and_last_color(color_list) 
