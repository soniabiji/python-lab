

def remove_even(list):
    for i in list: 
        if i % 2 == 0: 
            list.remove(i) 
    return list

print("Enter a list of integers separated by commas: ")
list = [int(i) for i in input().split(',')] 
print("List of integers after removing even numbers: ",remove_even(list))
