from datetime import date
print("Date:")
print(date.today())

import time
print("TIME:")
print(time.time())

import datetime
print("DATE AND TIME:")
a=datetime.datetime.now()
print(a)

import calendar
print("CALENDAR 2022:")
print(calendar.calendar(2022))

import math
n=int(input("Enter a number: "))
print("Square root of ",n,"is ",math.sqrt(n))
print("Sine of ",n,"is ",math.sin(n))
print("Tangent of ",n,"is ",math.tan(n))
print("Cosine of ",n,"is ",math.cos(n))


from math import pi
n=int(input("Enter a number: "))
print("Area of a circle with radius ",n,"is ",pi*n*n)

import string
print(string.ascii_letters)
print(string.ascii_lowercase)
print(string.ascii_uppercase)
print(string.digits)
print(string.whitespace)
